mes= str(input("ingrese un mes del año: ")).lower()

if mes in ('enero', 'febrero', 'marzo'):
    print(f"{mes} pertenese a la estacion verano ")
elif mes in ('abril', 'mayo', 'junio'):
    print(f"{mes} pertenese a la estacion otoño")
elif mes in ('julio', 'agosto', 'septiembre'):
    print(f"{mes} pertenese a la estacion invierno")
elif mes in ('octubre', 'noviembre', 'diciembre'):
    print(f"{mes} pertenese a la estacion primavera")