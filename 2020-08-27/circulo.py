from math import pi

def diametro (radio):
    diametro=pi*(radio**2)
    return diametro

def perimetro (radio):
    perimetro=(pi*2)*radio
    return perimetro

def area (radio):
    area=radio*2
    return area