from circulo import diametro, area, perimetro

radio= float(input("ingrese el radio de un circulo: "))

print(f"el diametro del circulo es: {diametro(radio)}")
print(f"el area del circulo es: {area(radio)}")
print(f"el perimetro del circulo es: {perimetro(radio)}")
