#cadenas simples
cadena1= 'una \"cadena" de texto'
cadena2= "una \'cadema' de texto"

#cadenas multiples lineas
cadena3= '''una cadena
de "varias"
lineas'''

cadena4= """una cadena
de 'varias'
lineas"""

print(cadena1)
print(cadena2)
print(cadena3)
print(cadena4)
