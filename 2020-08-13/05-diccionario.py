dicc = {
    "banana" : "amarrila",
    "manzana" : "roja",
    "pera" : "verde"
}

print(dicc)
print(dicc["banana"])
print(dicc.values())
print(dicc.keys())

dicc["banana"]= "otro color"
dicc["pera"]= 10
dicc.pop("manzana")
print(dicc)

