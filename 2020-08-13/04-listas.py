lista1= [0,5,3,-1,10,6,8,-4,-1]

print(len(lista1))
print(lista1)
lista1.append(45)
print(lista1)
lista1.insert(2,6)
print(lista1)
lista1.reverse()
print(lista1)
lista1.sort()
print(lista1)
lista1.sort(reverse=True)
print(lista1)
#quita por indice
lista1.pop(2)
print(lista1)
#quita por elemento
lista1.remove(-1)
print(lista1)
lista1.extend([-3, 7])
print(lista1)

print(5 in lista1)
